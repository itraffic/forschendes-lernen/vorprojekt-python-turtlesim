from enum import Enum


class Direction(Enum):
    LEFT = 1
    RIGHT = 2

    def __str__(self):
        ugly_str = super().__str__()
        return ugly_str[ugly_str.find('.') + 1:]
