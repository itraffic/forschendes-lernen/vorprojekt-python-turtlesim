from math import tan


# Calculate air resistance force
# The air resistance force (F_air) acting on the vehicle can be estimated using the following formula:
# F_air = C_air * v^2
# where C_air is the air resistance coefficient and v is the current velocity.
def calculate_air_resistance(current_velocity, air_resistance):
    return air_resistance * (current_velocity ** 2)


# Calculate friction force
# The friction force (F_friction) acting on the vehicle can be estimated using the following formula:
# F_friction = C_friction * v
# where C_friction is the friction coefficient and v is the current velocity.
def calculate_friction_force(current_velocity, friction):
    return friction * current_velocity


# Calculate total resistance force
# The total resistance force (F_resistance) is the sum of the air resistance and friction forces:
# F_resistance = F_air + F_friction
def calculate_resistance_force(current_velocity, air_resistance, friction):
    air_res = calculate_air_resistance(current_velocity, air_resistance)
    friction_force = calculate_friction_force(current_velocity, friction)
    return air_res + friction_force


# Calculate acceleration
# The acceleration (a) of the vehicle can be calculated using Newton's second law of motion:
# a = F_net / m
# where F_net is the net force acting on the vehicle and m is the mass of the vehicle.
def calculate_acceleration(net_force, mass):
    return net_force / mass


# Calculate new velocity
# The new velocity (v_new) can be calculated by integrating the acceleration (a) over the time step (time_since_last_tick):
# v_new = v + a * time_since_last_tick
# where v is the current velocity.
def calculate_new_velocity(current_velocity, acceleration, time_since_last_tick):
    return current_velocity + acceleration * time_since_last_tick


# Calculate angular velocity based on the bicycle model
# The angular velocity (w) can be calculated based on the bicycle model, which relates the linear velocity (v) to the steering angle (delta):
# w = v * tan(delta) / L
# where L is the wheelbase (distance between the front and rear axles) of the vehicle.
def calculate_angular_velocity(current_velocity, steering_angle, wheelbase_length):
    return current_velocity * tan(steering_angle) / wheelbase_length


def calculate_engine_acceleration_force(
        wheel_radius_m: float,
        gear_ratio: float,
        final_drive_ratio: float,
        avg_engine_torque_nm: float,
        engine_loss: float) -> float:
    engine_torque_after_transmission = avg_engine_torque_nm * gear_ratio * final_drive_ratio
    engine_torque_after_losses = engine_torque_after_transmission * engine_loss
    return engine_torque_after_losses * (1 / wheel_radius_m)


def calculate_engine_rpm_from_speed(
        speed_m_s: float,
        wheel_circumference_m: float,
        current_gear_ratio: float,
        final_drive_ratio: float) -> float:
    speed_m_min = speed_m_s * 60
    wheel_rpm = speed_m_min / wheel_circumference_m
    return wheel_rpm * final_drive_ratio * current_gear_ratio
