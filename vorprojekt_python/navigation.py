import math
from time import time as get_current_time
from rclpy.duration import Duration
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy
from scipy.interpolate import interp1d
from sensor_msgs.msg import LaserScan

from debug_renderer.laserscan_renderer import LaserScanRenderer
from direction import Direction


class Navigation:
    def __init__(self, robot):
        self.delay = 0.0
        self.robot = robot
        qos = QoSProfile(depth=1, reliability=QoSReliabilityPolicy.BEST_EFFORT, history=QoSHistoryPolicy.KEEP_LAST)
        self.laserscan = robot.create_subscription(LaserScan, '/scan', self.laser_callback, qos)
        self.detection_distance_m = robot.config.navigation.detection_distance_m
        self.obstacle_ahead = None
        self.angles = [0] * 360
        self.angles_history = WrappingList(5)
        self.laser_scan_renderer = LaserScanRenderer(100.0, marking_distance=self.detection_distance_m)
        self.low_latency_visualizer = False
        robot.visualizer.register_renderer(self.laser_scan_renderer)

    @staticmethod
    def rad_to_deg(rad):
        return 180 * rad / math.pi

    def laser_callback(self, msg: LaserScan):
        sec = get_current_time() - msg.header.stamp.sec
        sec -= msg.header.stamp.nanosec / 1_000_000_000
        self.delay = sec * 1000

        new_angles: [float] = [float('nan')] * 360

        # print("LEN: ", len(msg.ranges))
        # Set current angle as variable used in loop
        current_angle = msg.angle_min
        # For each index of received angles:
        for distance in msg.ranges:
            # Convert angle to degrees
            deg = int(self.rad_to_deg(current_angle))
            current_angle += msg.angle_increment
            # Sanity check distances
            if math.isnan(distance):
                # leave invalid values empty
                continue
            if math.isinf(distance):
                distance = msg.range_max

            new_angles[deg % 360] = distance

        # Create interpolation function to fill in missing values
        read_degree_indexes = []
        read_degree_distances = []
        # Fill arrays with measured values and indexes
        for i, distance in enumerate(new_angles):
            if not math.isnan(distance):
                read_degree_indexes.append(i)
                read_degree_distances.append(distance)

        average_distance = sum(read_degree_distances) / len(read_degree_distances)
        # Create interpolation function based on measured data
        interpolate_angles = interp1d(
            read_degree_indexes,
            read_degree_distances,
            fill_value=(read_degree_distances[0], read_degree_distances[-1]),
            kind="linear",
            bounds_error=False
        )

        # Use interpolation function to fill in missing values
        for i, distance in enumerate(new_angles):
            if math.isnan(distance):
                new_angles[i] = interpolate_angles(i)
                # print("Interp: ", i, new_angles[i])

        self.angles_history.append(new_angles)
        self.angles = self.angles_history.smoothed()
        if self.low_latency_visualizer:
            self.laser_scan_renderer.update(new_angles)
        else:
            self.laser_scan_renderer.update(self.angles)

        # print("AAAA:", end='')
        # for i in self.angles:
        #     print(i, end=',')
        # print("")

        # Check a few degrees at the front for objects within the detection distance
        left_angles = self.angles[0:20]
        right_angles = self.angles[340:359]
        min_distance = min(min(left_angles), min(right_angles))
        avg_distance = 0.0
        avg_count = 0
        max_distance = max(max(right_angles), max(right_angles))

        too_close = False

        for distance in left_angles:
            avg_distance += distance
            avg_count += 1
            too_close |= distance < self.detection_distance_m
        for distance in right_angles:
            avg_distance += distance
            avg_count += 1
            too_close |= distance < self.detection_distance_m
        # print('TOO CLOSE: ', too_close)
        avg_distance /= avg_count

        # print(f"MIN: {min_distance:.02}\tAVG: {avg_distance:.02}\tMAX: {max_distance:.02}")

        # Brake and turn when something is too close
        if too_close:
            if self.obstacle_ahead is None:
                # Decide where to turn based on what is the closest object in the front left and right 90 degrees
                if min(self.angles[0:60]) > min(self.angles[300:359]):
                    self.obstacle_ahead = Direction.LEFT
                else:
                    self.obstacle_ahead = Direction.RIGHT
                self.robot.update()
        elif self.obstacle_ahead is not None:
            self.obstacle_ahead = None
            self.robot.update()


class WrappingList(list):
    def __init__(self, max_elements):
        super().__init__()
        self.max_elements = max_elements
        self.append_index = 0

    def append(self, item):
        if len(self) < self.max_elements:
            super().append(item)
        else:
            super().__setitem__(self.append_index % self.max_elements, item)
        self.append_index += 1

    def smoothed(self):
        result = []
        for i in range(len(self[0])):
            result.append(sum([x[i] for x in self]) / len(self))
        return result
