import os
import sys


def fix_paths():
    my_robot_root = os.path.dirname(__file__)
    sys.path.insert(0, os.path.abspath(my_robot_root))
    # sys.path.insert(0, os.path.join(os.path.abspath(my_robot_root), ".."))
    # print("PATH:" + str(sys.path))
    pass


def main(args=None):
    fix_paths()
    import my_robot
    my_robot.start(args=args)


if __name__ == '__main__':
    main()
