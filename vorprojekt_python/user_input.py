import os
import select
import sys
from enum import Enum

from direction import Direction

if os.name == 'nt':
    import msvcrt
else:
    import termios
    import tty


class UserWish(Enum):
    BRAKE = 0
    DRIVE = 1
    FORCE_DRIVE = 2
    EXIT = 3

    def __str__(self):
        ugly_str = super().__str__()
        return ugly_str[ugly_str.find('.') + 1:]


class UserInput:

    def __init__(self, robot):
        self.reset_future = None
        self.robot = robot
        # self.gazebo_client = robot.create_client(Empty, '/reset_world')
        # self.gazebo_client.wait_for_service()
        self.drive_wish = UserWish.BRAKE
        self.turn_wish = None
        self.timer = robot.create_timer(0.1, self.timer_callback)

        # Settings
        self.settings = None
        if os.name != 'nt':
            self.settings = termios.tcgetattr(sys.stdin)

        print("Press 'W' to start or 'S' to brake or 'R' to reset the robot or 'X' to exit.")

    def get_key(self):
        if os.name == 'nt':
            return msvcrt.getch().decode('utf-8')
        tty.setraw(sys.stdin.fileno())
        rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
        if rlist:
            key = sys.stdin.read(1)
        else:
            key = ''
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.settings)
        return key

    def timer_callback(self):
        # don't do anything while the simulation is resetting
        if self.reset_future is not None and not self.reset_future.done():
            return

        # get key and react to it
        try:
            key = self.get_key()
            if key == 'w':
                print("Driving..")
                self.drive_wish = UserWish.DRIVE
                self.turn_wish = None
            if key == 'd':
                self.turn_wish = Direction.RIGHT
            if key == 'a':
                self.turn_wish = Direction.LEFT
            if key == 'f':
                print("Force driving..")
                self.drive_wish = UserWish.FORCE_DRIVE
            if key == 's':
                print("Braking..")
                self.drive_wish = UserWish.BRAKE
                self.turn_wish = None
            if key == 'r':
                print("Resetting..")
                # self.reset_future = self.gazebo_client.call_async(Empty.Request())
            if key == 'x' or key == '\x03':
                print("Exiting..")
                self.drive_wish = UserWish.EXIT
            self.robot.update()
        except Exception as e:
            print(e)
