from nav_msgs.msg import Odometry
from rclpy.qos import QoSReliabilityPolicy, QoSHistoryPolicy, QoSProfile

from config.configuration import ConstantsConfig


class Tracking:

    def __init__(self, robot):
        self.robot = robot
        self.config: ConstantsConfig = robot.config.constants

        qos = QoSProfile(depth=5, reliability=QoSReliabilityPolicy.BEST_EFFORT, history=QoSHistoryPolicy.KEEP_LAST)
        self.pose_subscriber = robot.create_subscription(
            Odometry, "/odom", self.pose_callback, qos
        )
        self.current_measured_velocity = 0.0
        self.seconds_since_last_print = 0.0

    def pose_callback(self, msg: Odometry):
        self.current_measured_velocity = msg.twist.twist.linear.x
        self.robot.update()
