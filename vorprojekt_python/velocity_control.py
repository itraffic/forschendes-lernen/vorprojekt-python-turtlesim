from time import time as get_current_time

from geometry_msgs.msg import Twist
from rclpy.qos import QoSProfile
from scipy.interpolate import interp1d

from config.configuration import Configuration
from velocity_calculations import *

# BURGER_MAX_LIN_VEL = 0.22
BURGER_MAX_LIN_VEL = 0.22
# BURGER_MAX_ANG_VEL = 2.84
BURGER_MAX_ANG_VEL = 2.8


def constrain(input_vel, low_bound, high_bound):
    if input_vel < low_bound:
        input_vel = low_bound
    elif input_vel > high_bound:
        input_vel = high_bound
    else:
        input_vel = input_vel

    return input_vel


def sanity_check(twist):
    twist.linear.x = constrain(twist.linear.x, -BURGER_MAX_LIN_VEL, BURGER_MAX_LIN_VEL)
    twist.angular.z = constrain(twist.angular.z, -BURGER_MAX_ANG_VEL, BURGER_MAX_ANG_VEL)


class VelocityControl:
    def __init__(self, robot):
        self.robot = robot
        self.config: Configuration = robot.config
        # qos = QoSProfile(depth=2, reliability=QoSReliabilityPolicy.RELIABLE, history=QoSHistoryPolicy.KEEP_LAST)
        qos = QoSProfile(depth=10)
        self.publisher = robot.create_publisher(Twist, '/cmd_vel', qos)
        self.twist = Twist()
        # self.timer = robot.create_timer(0.1, self.timer_callback)

        # Position
        self.last_tick = get_current_time()

        # Parameters
        self.target_velocity = 0.0
        self.current_velocity = 0.0
        self.steering_angle = 0.0
        self.current_scaled_velocity = 0.0
        self.current_rpm = 0.0
        self.current_gear = 0
        self.current_force = 0.0
        self.current_resistance = 0.0
        self.last_gear_switch_time = get_current_time()

    def calculate_tick(self):
        # Time since last tick
        time_since_last_tick = get_current_time() - self.last_tick
        self.last_tick = get_current_time()

        # print(self.target_velocity, self.current_velocity)
        # Does our velocity need to change?
        if abs(self.target_velocity - self.current_velocity) > self.config.constants.velocity_delta_threshold_m_s:
            self.recalculate_velocity(time_since_last_tick)

        # Always publish a twist, since external access to it from nav is needed to avoid obstacles
        self.twist.angular.z = calculate_angular_velocity(self.current_velocity, self.steering_angle,
                                                          self.config.car.wheelbase_length)  # handle steering
        # print(self.twist)
        sanity_check(self.twist)
        if not self.robot.manual_control:
            self.publisher.publish(self.twist)
        # print(self.twist)

    def recalculate_velocity(self, time_since_last_tick):
        # Are we braking or accelerating?
        is_braking = self.target_velocity < self.current_velocity
        scaled_velocity = self.current_velocity * self.config.constants.scale

        # print(
        #     f"{'Braking' if is_braking else 'Accelerating'} "
        #     f"Force: {self.config.car.braking_force if is_braking else self.config.car.acceleration_force}"
        # )
        # Calculate air resistance and friction
        self.current_resistance = calculate_resistance_force(
            scaled_velocity,
            self.config.car.air_resistance,
            self.config.car.friction)

        torque_interpolation = interp1d(
            self.config.car.engine.speed_points_full_load_rpm,
            self.config.car.engine.static_torque_points_full_load_nm,
            kind="linear",
            fill_value=(
                self.config.car.engine.static_torque_points_full_load_nm[0],
                self.config.car.engine.static_torque_points_full_load_nm[-1]
            ),
            bounds_error=False
        )

        self.current_rpm = calculate_engine_rpm_from_speed(
            scaled_velocity,
            self.config.car.wheel_circumference_m,
            self.config.car.gears[self.current_gear].ratio,
            self.config.car.engine.final_drive_ratio
        )

        current_torque = torque_interpolation(self.current_rpm)

        if get_current_time() > self.last_gear_switch_time + self.config.car.gear_switch_time_s:
            self.current_force = calculate_engine_acceleration_force(
                self.config.car.wheel_radius_m,
                self.config.car.gears[self.current_gear].ratio,
                self.config.car.engine.final_drive_ratio,
                current_torque,  # Use the interpolated torque
                self.config.car.engine.avg_loss
            )
            if self.current_rpm > self.config.car.gears[self.current_gear].end_speed_rpm and \
                    len(self.config.car.gears) > self.current_gear + 1:
                self.current_gear += 1
                self.last_gear_switch_time = get_current_time()
            elif self.current_rpm < self.config.car.gears[self.current_gear].start_speed_rpm:
                self.current_gear -= 1
                self.last_gear_switch_time = get_current_time()

        else:
            self.current_force = 0.0


        # Do we need braking-force or accelerating-force?
        # When braking, the resistance adds to the braking force.
        # When accelerating, the resistance subtracts from it.
        if is_braking:
            force = -(max(0.01, self.config.car.braking_force) + self.current_resistance)
        else:
            force = max(0.01, self.current_force) - self.current_resistance

        # Calculate acceleration
        acceleration = calculate_acceleration(force, self.config.car.mass_kg)

        # Calculate new velocity
        tmp_velocity = calculate_new_velocity(scaled_velocity, acceleration, time_since_last_tick)

        # If we are braking, we want to stop at 0
        if is_braking and tmp_velocity <= self.config.constants.standing_still_threshold_m_s:
            tmp_velocity = 0.0
        # If we are above the target velocity while accelerating, clamp it to the target speed
        elif not is_braking and tmp_velocity > self.target_velocity * self.config.constants.scale:
            tmp_velocity = self.target_velocity * self.config.constants.scale

        # print(f"Resistance: {resistance:.02}\tForce: {force:.02}\tAcc: {acceleration:.02}\tVel T: {tmp_velocity:.02}\tVel L: {self.current_velocity:.02}")

        # Apply the new velocity parameter to the actual turtlebot
        self.current_scaled_velocity = tmp_velocity
        self.current_velocity = tmp_velocity / self.config.constants.scale
        self.twist.linear.x = self.current_velocity
