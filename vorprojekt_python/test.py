import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

from config.config_reader import read_config
from velocity_calculations import *

# car = read_car_config("../../resource/config/default_car")
config = read_config("../../resource/config/16MY_Jaguar_FType")
car = config.car

time_step = 0.01
total_time = 10.0
gear_switch_time = 0.2

current_time = 0.0
current_force = 0.0
current_acceleration = 0.0
current_speed = 0.0
current_gear = 0
current_rpm = 0
last_gear_switch_time = -gear_switch_time

time = []
rpm = []
force = []
acceleration = []
speed = []
gear = []

while current_time < total_time:
    time.append(current_time)
    force.append(current_force)
    acceleration.append(current_acceleration)
    speed.append(current_speed)
    gear.append(current_gear)
    current_time += time_step

    torque_interpolation = interp1d(
        car.engine.speed_points_full_load_rpm,
        car.engine.static_torque_points_full_load_nm,
        kind="linear",
        fill_value=(car.engine.static_torque_points_full_load_nm[0], car.engine.static_torque_points_full_load_nm[-1]),
        bounds_error=False
    )

    current_torque = torque_interpolation(current_rpm)

    current_force = calculate_engine_acceleration_force(
        car.wheel_radius_m,
        car.gears[current_gear].ratio,
        car.engine.final_drive_ratio,
        current_torque,  # Use the interpolated torque
        car.engine.avg_loss
    )

    air_resistance = calculate_air_resistance(current_speed, car.air_resistance)
    friction = calculate_friction_force(current_speed, car.friction)

    if current_time > last_gear_switch_time + gear_switch_time:
        current_acceleration = calculate_acceleration(current_force - air_resistance - friction, car.mass_kg)
        current_rpm = calculate_engine_rpm_from_speed(
            current_speed,
            car.wheel_circumference_m,
            car.gears[current_gear].ratio,
            car.engine.final_drive_ratio
        )
    else:
        current_acceleration = 0.0
        current_rpm = 0.0

    current_speed = calculate_new_velocity(current_speed, current_acceleration, time_step)
    rpm.append(current_rpm)

    if current_time > last_gear_switch_time + gear_switch_time:
        if current_rpm > car.gears[current_gear].end_speed_rpm and len(car.gears) > current_gear + 1:
            current_gear += 1
            last_gear_switch_time = current_time
        elif current_rpm < car.gears[current_gear].start_speed_rpm:
            current_gear -= 1
            last_gear_switch_time = current_time

figure, axis = plt.subplots(3, 2)

axis[0, 0].plot(time, rpm)
axis[0, 0].set_xlabel("Time [s]")
axis[0, 0].set_ylabel("RPM [1/min]")
axis[0, 0].set_title("RPM over Time")

axis[0, 1].plot(time, force)
axis[0, 1].set_xlabel("Time [s]")
axis[0, 1].set_ylabel("Force [N]")
axis[0, 1].set_title("F over Time")

axis[1, 0].plot(time, acceleration)
axis[1, 0].set_xlabel("Time [s]")
axis[1, 0].set_ylabel("Acc. [m/s^2]")
axis[1, 0].set_title("Acceleration over Time")

axis[1, 1].plot(time, [x + 1 for x in gear])
axis[1, 1].set_xlabel("Time [s]")
axis[1, 1].set_ylabel("Gear [1]")
axis[1, 1].set_title("Gear over Time")

axis[2, 0].plot(time, speed)
axis[2, 0].set_xlabel("Time [s]")
axis[2, 0].set_ylabel("Speed [m/s]")
axis[2, 0].set_title("Speed over Time")

axis[2, 1].plot(time, [x * 3.6 for x in speed])
axis[2, 1].set_xlabel("Time [s]")
axis[2, 1].set_ylabel("Speed [km/h]")
axis[2, 1].set_title("Speed over Time")

# using padding
figure.tight_layout(pad=1.5)
figure.set_figwidth(6.4)
figure.set_figheight(6.4)

# Combine all the operations and display
plt.show()
