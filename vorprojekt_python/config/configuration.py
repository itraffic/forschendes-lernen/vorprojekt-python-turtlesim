from dataclasses import dataclass
from typing import List

from yaml import YAMLObject


@dataclass
class ConstantsConfig(YAMLObject):
    timer_tick_s: float
    standing_still_threshold_m_s: float
    velocity_delta_threshold_m_s: float
    velocity_printing_interval_s: float
    wheelbase_length: float
    scale: float


@dataclass
class NavigationConfig(YAMLObject):
    detection_distance_m: float


@dataclass
class GearConfig(YAMLObject):
    start_speed_rpm: float
    end_speed_rpm: float
    ratio: float


@dataclass
class EngineConfig(YAMLObject):
    max_torque_nm: float
    speed_at_maximum_torque_rpm: float
    maximum_power_ps: float
    speed_at_maximum_power_rpm: float
    final_drive_ratio: float
    avg_torque: float
    avg_loss: float
    speed_points_full_load_rpm: List[int]
    static_torque_points_full_load_nm: List[int]


@dataclass
class CarConfig(YAMLObject):
    steering_angle: float
    wheelbase_length: float
    wheel_radius_m: float
    wheel_circumference_m: float
    braking_force: float
    acceleration_force: float
    target_speed_m_s: float
    mass_kg: float
    air_resistance: float
    friction: float
    aerodynamic_drag: float
    frontal_area_m2: float
    maximum_speed_kph: float
    acceleration_time_0_100_kph_s: float
    gear_switch_time_s: float
    gears: List[GearConfig]
    engine: EngineConfig


@dataclass
class Configuration(YAMLObject):
    constants: ConstantsConfig
    navigation: NavigationConfig
    car: CarConfig
