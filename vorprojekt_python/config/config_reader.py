import os

import yaml

from config.configuration import Configuration

CONFIGS_PATH = os.path.abspath(os.path.dirname(__file__)) + '/'


def read_config(name: str) -> Configuration:
    with open(CONFIGS_PATH + name + ".yml", "r") as file:
        return yaml.load(file, Loader=yaml.UnsafeLoader)
