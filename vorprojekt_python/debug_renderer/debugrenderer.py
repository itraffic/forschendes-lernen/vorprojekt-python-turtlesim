from abc import ABC, abstractmethod

from sdl2.ext import Renderer, Window


class DebugRenderer(ABC):
    @abstractmethod
    def render(self, renderer: Renderer, window: Window, sdl2debug):
        pass
