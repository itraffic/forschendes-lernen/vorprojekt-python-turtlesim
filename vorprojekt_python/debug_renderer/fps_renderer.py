from time import time

from debug_renderer.text_renderer import TextRenderer


class FPSRenderer(TextRenderer):
    def __init__(self, ):
        super().__init__(text_func=self.get_fps)
        self.time = time()

    def get_fps(self) -> str:
        elapsed_time = time() - self.time
        self.time = time()
        fps = 1.0 / elapsed_time
        return "FPS: {}".format(int(fps))
