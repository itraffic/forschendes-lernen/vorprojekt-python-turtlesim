import ctypes
from threading import Thread
from time import time

import sdl2
import sdl2.ext

from debug_renderer.debugrenderer import DebugRenderer
from debug_renderer.joystick_renderer import JoystickRenderer
from direction import Direction
from user_input import UserWish


class SDL2Debug(Thread):
    def __init__(self, fps=30.0, width=1280, height=920):
        super().__init__(daemon=True)
        self.fps = fps
        sdl2.ext.init(joystick=True)
        # Check for available joysticks
        num_joysticks = sdl2.SDL_NumJoysticks()
        print(f"Number of joysticks: {num_joysticks}")

        # Open all available joysticks
        # TODO: Make this less crude
        for i in range(num_joysticks):
            joystick = sdl2.SDL_JoystickOpen(i)
            if joystick:
                print(f"Joystick {i} connected")
                self.joystick_renderer = JoystickRenderer(sdl2.SDL_JoystickOpen(i), width, height)
        self.robot = None
        self.window = sdl2.ext.Window("Turtlebot Surroundings", size=(width, height))
        self.window.show()
        self.renderer = sdl2.ext.Renderer(self.window)
        self.renderer.color = sdl2.ext.Color(0, 0, 200)
        self.running = True
        self.sub_renderers: [DebugRenderer] = []

    def register_renderer(self, renderer: DebugRenderer):
        self.sub_renderers.append(renderer)

    def run(self):
        last_frame_time = time()
        while self.running:
            self.renderer.clear(sdl2.ext.Color(0, 0, 0))
            for r in self.sub_renderers:
                r.render(self.renderer, self.window, self)

            self.renderer.present()
            for event in sdl2.ext.get_events():
                if event.type == sdl2.SDL_QUIT:
                    self.running = False
                    sdl2.ext.quit()
                    self.robot.running = False
                    break
                elif event.type == sdl2.SDL_KEYDOWN:
                    match event.key.keysym.sym:
                        case sdl2.SDLK_UP | sdl2.SDLK_w:
                            self.robot.user_input.drive_wish = UserWish.DRIVE
                            self.robot.user_input.turn_wish = None
                        case sdl2.SDLK_DOWN | sdl2.SDLK_s:
                            self.robot.user_input.drive_wish = UserWish.BRAKE
                            self.robot.user_input.turn_wish = None
                        case sdl2.SDLK_RIGHT | sdl2.SDLK_d:
                            self.robot.user_input.turn_wish = Direction.RIGHT
                        case sdl2.SDLK_LEFT | sdl2.SDLK_a:
                            self.robot.user_input.turn_wish = Direction.LEFT
                        case sdl2.SDLK_f:
                            self.robot.user_input.drive_wish = UserWish.FORCE_DRIVE
                        case sdl2.SDLK_PLUS | sdl2.SDLK_EQUALS | sdl2.SDLK_KP_PLUS:
                            self.robot.target_speed_m_s += 0.02
                        case sdl2.SDLK_MINUS | sdl2.SDLK_KP_MINUS:
                            self.robot.target_speed_m_s -= 0.02
                        case sdl2.SDLK_x | sdl2.SDLK_c | sdl2.SDLK_q:
                            self.robot.user_input.drive_wish = UserWish.EXIT
                        case sdl2.SDLK_m:
                            self.toggle_manual_control()
                        case sdl2.SDLK_l:
                            self.robot.nav.low_latency_visualizer = not self.robot.nav.low_latency_visualizer
                elif event.type == sdl2.SDL_JOYAXISMOTION:
                    self.joystick_renderer.update()
                elif event.type == sdl2.SDL_JOYBUTTONDOWN:
                    match event.jbutton.button:
                        case 6:
                            self.robot.user_input.drive_wish = UserWish.EXIT
                        case 7:
                            self.toggle_manual_control()
                elif event.type == sdl2.SDL_JOYDEVICEADDED:
                    joystick = sdl2.SDL_JoystickOpen(event.jdevice.which)
                    if joystick:
                        print(f"Joystick {event.jdevice.which} connected")
                elif event.type == sdl2.SDL_JOYDEVICEREMOVED:
                    sdl2.SDL_JoystickClose(event.jdevice.which)
                    print(f"Joystick {event.jdevice.which} disconnected")
            frame_duration = time() - last_frame_time
            expected_frame_duration = 1.0 / self.fps
            if frame_duration < expected_frame_duration:
                delay_seconds = expected_frame_duration - frame_duration
                sdl2.SDL_Delay(int(delay_seconds * 1000.0))
            last_frame_time = time()

    def toggle_manual_control(self):
        self.robot.manual_control = not self.robot.manual_control
        if self.robot.manual_control and self.joystick_renderer not in self.sub_renderers:
            self.sub_renderers.append(self.joystick_renderer)
            self.joystick_renderer.update()
        else:
            self.sub_renderers.remove(self.joystick_renderer)

    def quit(self):
        self.running = False
        sdl2.ext.quit()
