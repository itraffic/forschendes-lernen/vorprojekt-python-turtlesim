from math import cos, sin

from sdl2.ext import Renderer, Window, Color

from debug_renderer.debugrenderer import DebugRenderer


class LaserScanRenderer(DebugRenderer):

    def __init__(self, scale: float = 1.0, color=Color(0, 0, 255), marking_distance=0.3,
                 marking_color=Color(255, 0, 255), self_color=Color(0, 200, 0)):
        self.scale = scale
        self.color = color
        self.marking_distance = marking_distance
        self.marking_color = marking_color
        self.self_color = self_color
        self.lengths = [0.0] * 360

    def update(self, lengths: list[float]):
        if len(lengths) != 360:
            raise RuntimeError(f"Expected 360 lengths, got {len(lengths)}!")
        self.lengths = lengths

    def render(self, renderer: Renderer, window: Window, sdl2debug):
        width, height = window.size
        center_x = width / 2.0
        center_y = height / 2.0
        for i, length in enumerate(self.lengths):
            angle = (270 - i) * 2 * 3.14159 / 360.0  # convert degree to radian
            end_x = int(center_x + length * self.scale * cos(angle))
            end_y = int(center_y + length * self.scale * sin(angle))
            if length < self.marking_distance:
                renderer.draw_line(points=[(center_x, center_y), (end_x, end_y)], color=self.marking_color)
            else:
                renderer.draw_line(points=[(center_x, center_y), (end_x, end_y)], color=self.color)
        renderer.fill(rects=[(center_x - 5, center_y - 5, 10, 10)], color=self.self_color)
        renderer.draw_line(points=[(center_x, center_y), (center_x, center_y - 200)], color=self.self_color)
