from sdl2.ext import Color

from debug_renderer.text_renderer import TextRenderer


class StaticTextRenderer(TextRenderer):

    def __init__(self, text: str, x: int = 0, y: int = 0, font_size: int = 16,
                 color=Color(255, 255, 255)):
        super().__init__(self.print_text, x, y, font_size, color)
        self.text = text

    def print_text(self):
        return self.text
