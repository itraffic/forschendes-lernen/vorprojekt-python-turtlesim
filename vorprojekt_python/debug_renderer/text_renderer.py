import os
from typing import Callable

import sdl2
from sdl2.ext import Renderer, Window, Texture, FontManager, Color

from debug_renderer.debugrenderer import DebugRenderer

FONT_PATH = os.path.abspath(os.path.dirname(__file__)) + '/../fonts/SourceCodePro-Black.ttf'


class TextRenderer(DebugRenderer):
    font_manager: FontManager = FontManager(font_path=FONT_PATH)

    def __init__(self, text_func: Callable[[], str], x: int = 0, y: int = 0, font_size: int = 16,
                 color=Color(255, 255, 255)):
        self.x = x
        self.y = y
        self.text_func = text_func
        self.color = color
        self.font_size = font_size

    def render(self, renderer: Renderer, window: Window, sdl2debug):
        fps_text = TextRenderer.font_manager.render(text=self.text_func(), color=self.color)
        fps_text_texture = Texture(renderer, fps_text)
        renderer.copy(fps_text_texture, dstrect=(self.x, self.y))
        fps_text_texture.destroy()
        sdl2.SDL_FreeSurface(fps_text)
