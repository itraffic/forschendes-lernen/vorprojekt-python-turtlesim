import sdl2
from geometry_msgs.msg import Twist
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy
from sdl2.ext import Renderer, Window, Color

from debug_renderer.debugrenderer import DebugRenderer


class JoystickRenderer(DebugRenderer):

    def __init__(self, joystick, width: int, height: int, max_turn_speed: float = 1.0, color: Color = Color(255, 0, 0)):
        self.publisher = None
        self.robot = None
        self.joystick = joystick
        self.max_turn_speed = max_turn_speed
        self.color = color
        self.center = (width // 2, height // 2)
        self.joystick_pos = (width // 2, height // 2)
        self.joystick_size = 5
        self.joystick_scale = 50

    def update(self):
        x_axis = sdl2.SDL_JoystickGetAxis(self.joystick, 0)
        y_axis = sdl2.SDL_JoystickGetAxis(self.joystick, 1)
        joystick_intensity = (x_axis / 32768, y_axis / 32768)
        dot = (int(joystick_intensity[0] * self.joystick_scale), int(joystick_intensity[1] * self.joystick_scale))
        self.joystick_pos = ((dot[0] + self.center[0]), dot[1] + self.center[1])
        if self.publisher is not None and self.robot.manual_control:
            twist = Twist()
            twist.linear.x = -joystick_intensity[1] * self.robot.target_speed_m_s
            twist.angular.z = -joystick_intensity[0] * self.max_turn_speed
            self.publisher.publish(twist)

    def render(self, renderer: Renderer, window: Window, sdl2debug):
        if sdl2debug.robot is not None and self.robot is None:
            self.robot = sdl2debug.robot
            qos = QoSProfile(depth=2, reliability=QoSReliabilityPolicy.RELIABLE, history=QoSHistoryPolicy.KEEP_LAST)
            self.publisher = self.robot.create_publisher(Twist, '/cmd_vel', qos)
        if sdl2debug.robot.manual_control:
            offset = self.joystick_size // 2
            renderer.fill(rects=[(
                self.joystick_pos[0] - offset, self.joystick_pos[1] - offset,
                self.joystick_size, self.joystick_size
            )], color=self.color)
            renderer.draw_line(points=[self.center, self.joystick_pos], color=self.color)
            renderer.fill(rects=[(
                self.center[0] - offset, self.center[1] - offset,
                self.joystick_size, self.joystick_size
            )], color=self.color)
