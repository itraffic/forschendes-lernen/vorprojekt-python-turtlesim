from statistics import mean

from rclpy.qos import QoSHistoryPolicy, QoSReliabilityPolicy, QoSProfile
from sdl2.ext import Color
from sensor_msgs.msg import BatteryState

from debug_renderer.text_renderer import TextRenderer


class BatteryRenderer(TextRenderer):

    def __init__(self, robot, x: int = 0, y: int = 0, font_size: int = 16,
                 color=Color(255, 255, 255), smoothing=True):
        super().__init__(self.get_battery_percent, x, y, font_size, color)
        self.x = x
        self.y = y
        self.font_size = font_size
        self.color = color
        self.smoothing = smoothing
        qos = QoSProfile(depth=2, reliability=QoSReliabilityPolicy.BEST_EFFORT, history=QoSHistoryPolicy.KEEP_LAST)
        self.battery_percentages: list[float] = [0.0] * 10
        self.index = 0
        self.subscriber = robot.create_subscription(BatteryState, '/battery_state', self.battery_callback, qos)

    def battery_callback(self, msg: BatteryState):
        self.battery_percentages[self.index] = msg.percentage
        self.index += 1
        self.index %= 10

    def get_battery_percent(self):
        return f"Battery: {int(mean(self.battery_percentages))} %"
