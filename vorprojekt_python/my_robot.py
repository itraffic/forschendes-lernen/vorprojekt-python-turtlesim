import rclpy
from geometry_msgs.msg import Twist
from rclpy.node import Node

from config.config_reader import read_config
from config.configuration import Configuration
from debug_renderer.battery_renderer import BatteryRenderer
from debug_renderer.fps_renderer import FPSRenderer
from debug_renderer.sdl2_debug import SDL2Debug
from debug_renderer.static_text_renderer import StaticTextRenderer
from debug_renderer.text_renderer import TextRenderer
from navigation import Navigation, Direction
from tracking import Tracking
from user_input import UserInput, UserWish
from velocity_control import VelocityControl


class MyRobot(Node):
    def __init__(self, visualizer: SDL2Debug):
        super().__init__('my_robot')
        self.manual_control = False
        self.visualizer = visualizer
        visualizer.robot = self
        self.config: Configuration = read_config("16MY_Jaguar_FType")

        self.user_input = UserInput(self)
        self.nav = Navigation(self)
        self.velocity_control = VelocityControl(self)
        self.tracking = Tracking(self)
        self.running = True
        self.steering_angle = self.config.car.steering_angle
        self.target_speed_m_s = self.config.car.target_speed_m_s
        self.initialize_debugger(visualizer)

    def initialize_debugger(self, visualizer):
        visualizer.register_renderer(FPSRenderer())
        visualizer.register_renderer(TextRenderer(
            lambda: f"Obstacle ahead  : {self.nav.obstacle_ahead}", x=500, y=0))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Front Distance  : {self.nav.angles[0] * 100:.1f} cm", x=500, y=20))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Back Distance   : {self.nav.angles[180] * 100:.1f} cm", x=500, y=40))
        visualizer.register_renderer(TextRenderer(
            lambda: f"User drive wish : {self.user_input.drive_wish}", x=500, y=880))
        visualizer.register_renderer(TextRenderer(
            lambda: f"User turn wish  : {self.user_input.turn_wish}", x=500, y=900))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Delay        : {self.nav.delay:.1f} ms", x=1020, y=720))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Force        : {self.velocity_control.current_force:.1f} N", x=1020, y=740))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Resistance   : {self.velocity_control.current_resistance:.1f} N", x=1020, y=760))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Scaled speed : {(self.velocity_control.current_scaled_velocity * 3.6):.1f} km/h", x=1020, y=780))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Scaled speed : {self.velocity_control.current_scaled_velocity:.1f} m/s", x=1020, y=800))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Gear         : {self.velocity_control.current_gear + 1} / {len(self.config.car.gears)}", x=1020,
            y=820))
        visualizer.register_renderer(TextRenderer(
            lambda: f"RPM          : {int(self.velocity_control.current_rpm)}", x=1020, y=840))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Speed        : {self.velocity_control.current_velocity:.03}", x=1020, y=860))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Target speed : {self.velocity_control.target_velocity:.3f}", x=1020, y=880))
        visualizer.register_renderer(TextRenderer(
            lambda: f"Max speed    : {self.target_speed_m_s:.3f}", x=1020, y=900))
        visualizer.register_renderer(BatteryRenderer(self, x=1145, y=0))

        visualizer.register_renderer(StaticTextRenderer(x=10, y=700, text="W: Losfahren"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=720, text="S: Bremsen"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=740, text="A: Links abbiegen"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=760, text="D: Rechts abbiegen"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=780, text="F: Losfahren, auch wenn vor Hindernis"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=800, text="X, C, Q: Sofort stoppen und beenden"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=820, text="+: Target Speed += 0.02"))
        visualizer.register_renderer(StaticTextRenderer(x=10, y=840, text="-: Target Speed -= 0.02"))
        visualizer.register_renderer(TextRenderer(
            lambda: f"M: Toggle Manual Control ({str(self.manual_control)})", x=10, y=860))
        visualizer.register_renderer(TextRenderer(
            lambda: f"L: Toggle low latency visuals ({str(self.nav.low_latency_visualizer)})", x=10, y=880))

    def update(self):
        tmp_steering_angle = 0.0
        tmp_target_velocity = 0.0

        # Prioritize user wish over navigation obstacles
        match self.user_input.turn_wish:
            case Direction.LEFT:
                tmp_steering_angle = self.steering_angle
            case Direction.RIGHT:
                tmp_steering_angle = -self.steering_angle
            case None:
                match self.nav.obstacle_ahead:
                    case None:
                        tmp_steering_angle = 0.0
                        tmp_target_velocity = self.target_speed_m_s
                    case Direction.LEFT:
                        tmp_steering_angle = self.steering_angle
                        tmp_target_velocity = self.target_speed_m_s / 2
                    case Direction.RIGHT:
                        tmp_steering_angle = -self.steering_angle
                        tmp_target_velocity = self.target_speed_m_s / 2

        # Handle User Input
        match self.user_input.drive_wish:
            case UserWish.FORCE_DRIVE:
                tmp_target_velocity = self.target_speed_m_s
            case UserWish.BRAKE:
                tmp_steering_angle = 0.0
                tmp_target_velocity = 0.0
            case UserWish.DRIVE:
                if self.nav.obstacle_ahead is None:
                    tmp_target_velocity = self.target_speed_m_s
            case UserWish.EXIT:
                self.running = False

        self.velocity_control.target_velocity = tmp_target_velocity
        self.velocity_control.steering_angle = tmp_steering_angle
        self.velocity_control.calculate_tick()

    def stop_robot(self):
        print("  Stopping robot...", end='')
        publisher = self.create_publisher(Twist, '/cmd_vel', 10)
        twist = Twist()
        twist.linear.x = 0.0
        twist.angular.z = 0.0
        publisher.publish(twist)
        print("Done!")


def start(args=None):
    rclpy.init(args=args)
    visualizer = SDL2Debug()
    my_robot = MyRobot(visualizer)
    visualizer.start()

    while my_robot.running:
        rclpy.spin_once(my_robot, timeout_sec=1)

    my_robot.stop_robot()

    print('  Shutting down rclpy...', end='')
    rclpy.shutdown()
    print('Done!')
