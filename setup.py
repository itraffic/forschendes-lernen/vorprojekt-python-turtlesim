from os import path

from setuptools import setup
from setuptools.glob import glob

package_name = 'vorprojekt_python'


def get_python_version():
    import sys
    return 'python' + str(sys.version_info.major) + '.' + str(sys.version_info.minor)


setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        # Package.xml
        (
            'share/' + package_name,
            [
                'package.xml'
            ]
        ),
        # Marker (required by colcon)
        (
            'share/ament_index/resource_index/packages',
            [
                'resource/' + package_name,
            ]
        ),
        # Code subdirectories
        (
            path.join('lib', get_python_version(), 'site-packages', package_name),
            glob(package_name + '/*/*.py')
        ),
        # Config files
        (
            path.join('lib', get_python_version(), 'site-packages', package_name, 'config'),
            glob('resource/config/*.yml')
        ),
        # Fonts files
        (
            path.join('lib', get_python_version(), 'site-packages', package_name, 'fonts'),
            glob('resource/fonts/*.ttf')
        ),
        # Builtin
        (
            path.join('lib', get_python_version(), 'site-packages', package_name),
            [
                'resource/' + package_name
            ]
        ),
    ],
    include_package_data=True,
    install_requires=['setuptools'],
    requires=['pyyaml'],
    zip_safe=False,
    maintainer='Vorprojekt Python',
    maintainer_email='vorprojekt-python@uol.de',
    description='Vorprojekt Python',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'main = vorprojekt_python.main:main'
        ],
    },
)
